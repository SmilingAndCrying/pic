#!/usr/bin/bash

LOG="/root/NFS.log"

yum install -y rpcbind && systemctl start rpcbind && systemctl enable rpcbind 
if [[ $? -eq 0 ]];then 
	echo "install server succ" >> $LOG
else
	echo "install server err" >> $LOG
	exit 1
fi

yum install -y nfs-utils && systemctl start nfs && systemctl enable nfs
if [[ $? -eq 0 ]];then 
	echo "start server succ" >> $LOG
else
	echo "start server err" >> $LOG
	exit 1
fi


mkdir /opt/share1 && chmod -R o+w /opt/share1
if [[ $? -eq 0 ]];then 
	echo "mkdir and chmod succ" >> $LOG
else
	echo "mkdir and chmod  err" >> $LOG
	exit 1
fi



echo "/opt/share1 ${1}(sync,rw) ${2}(sync,rw)" >> /etc/exports
if [[ $? -eq 0 ]];then 
	echo "{$1} ${2} into conf file succ" >> $LOG
else
	echo "{$1} ${2} into conf file  err" >> $LOG
	exit 1
fi





systemctl restart rpcbind
systemctl restart nfs
exportfs



systemctl start firewalld
firewall-cmd --permanent --zone=public --add-port=22/tcp
firewall-cmd --permanent --zone=public --add-service=nfs
useradd nullnull
echo "123456"|passwd nullnull --stdin
echo "nullnull ALL=(ALL) ALL" >> /etc/sudoers
sed -i "s/#PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config

